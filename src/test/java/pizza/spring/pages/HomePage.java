package pizza.spring.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HomePage {
    private WebDriver webDriver;

    public HomePage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public static HomePage openWith(WebDriver webDriver) {
        HomePage homePage = new HomePage(webDriver);
        homePage.open();
        return homePage;
    }

    public HomePage open() {
        webDriver.navigate().to("http://localhost:8080/pizza-spring/");
        assertEquals("Bienvenue dans l'application de gestion de vos commandes de pizzas.", webDriver.findElement(By.tagName("div")).getText());
        return this;
    }

    public CommandPage clickOnCommand() {
        WebElement commandButton = webDriver.findElement(By.cssSelector("#menu > ul > li:nth-child(1) > a"));
        commandButton.click();
        return new CommandPage(webDriver);
    }
}
